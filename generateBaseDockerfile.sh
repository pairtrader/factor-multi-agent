#! /bin/sh
for package in $(cat install.txt);
do  
    printf "\nRUN Rscript -e \"chooseCRANmirror(graphics=FALSE,66);install.packages('$package', dependencies = TRUE);require($package)\"" >> Dockerfile.factor_1.base 
    printf "\nRUN Rscript -e \"chooseCRANmirror(graphics=FALSE,66);install.packages('$package', dependencies = TRUE);require($package)\"" >> Dockerfile.factor_2.base 
done
