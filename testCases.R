#Title :  Test Cases Factor Analysis
#Created : July 26, 2019 Niharika
#version : 19.06.26 
# This code performs various tests on factor agent
# Revisions :
#   Version : 20.03.01  
#   Date : March 04, 2020
#   Author : Chirag Khatri
#   Changes:
#     Update the factor loadings vector used for comparison
#     Raising an error if one or more test cases failed
# Revisions :
#   Version : 21.03.01  
#   Date : March 10, 2021
#   Author : Nishit Jain
#   Changes:
#     Update test cases for two parallel factor agents
#------------------------------------------------------------------------------------------------------------------------------------------

# Loading required packages 
library(testthat)
library(xts)
library(jsonlite)
library(tidyverse)

# Loading R file in the session; to load Factor Analysis files
source("main_agent1.R")
source("main_agent2.R")

###
# Check for timestep
n.timestep = 38
###

# Loading sample raw KAFKA data (at least 40 rows in dataset are necessary to run factanal())
from_kafka <- readLines("Data/raw_prices.log.txt")
from_kafka <- from_kafka[1:50]
testCase <- list()

# Get n.factors and eventTimes from Factor Agent 1
# results created for testing on processing variables
data <- list()
for(i in 1:50){
  data[[i]] <- factor_agent1(from_kafka[i])
}

# Get JSON output for timestep 38 from Factor Agent 1
from_factor_agent_1 <- jsonlite::toJSON(list(n.factors = data[[n.timestep]]$n.factors,
                                             factorEventTime = data[[n.timestep]]$factorEventTime),
                                        force=TRUE,
                                        rownames=FALSE)

# Run Factor Agent 2 for the selected timestep (this agent should take raw prices till step n.timestep + 1, as it takes
# n.factors from step n.timestep
factor_output <- factor_agent2(from_factor_agent_1)

# variable to store benchmarked values for output
factor_loadings <- c(0.262704, 0.244422, 0.180793, 0.054385, 0.050156, 0.032213, 0.028381, 0.024740, 0.022246, 0.009905)

# Variable to save results
allResults <- list()
allResultsSummary <- list()

##### An individual factor must not explain majority of variance (Threshold 90%)
testCaseOne <- function(data){
  failedTestResults <- list()
  result <- tryCatch({
    test_that("No individual factor should explain 90% variance",{
      
      less_than = function(x){ x < 0.9}
      value <- sapply(data$proportionalVar, less_than)
      check <- which(value == FALSE) 
      if(length(check)==0){
        testCaseOne_Check <- TRUE
      }
      expect_equal(testCaseOne_Check, TRUE)
    })
  }, error = function(e){return(e)})
  if(!isTRUE(result)){
    failedTestResults <- c(failedTestResults, as.character(result))
  }
  
  if(length(failedTestResults) == 0){
    return("Factor Analysis testCaseOne Passed")
  } else {
    return(failedTestResults)
  }
}

testCaseOneResults <- testCaseOne(factor_output)
if(testCaseOneResults != 'Factor Analysis testCaseOne Passed') {
  allResults <- c(allResults,'testCaseOne', testCaseOneResults)
}

##### Eventime should be present in output JSON
testCasetwo <- function(data){
  failedTestResults <- list()
  result <- tryCatch({
    test_that("Eventime is present in output JSON",{
       
      if(!is.null(data$factorEventTime)){
        testCasetwo_Check <- TRUE
      }
      expect_equal(testCasetwo_Check, TRUE)
    })
  }, error = function(e){return(e)})
  if(!isTRUE(result)){
    failedTestResults <- c(failedTestResults, as.character(result))
  }
  
  if(length(failedTestResults) == 0){
    return("Factor Analysis testCasetwo Passed")
  } else {
    return(failedTestResults)
  }
}

testCasetwoResults <- testCasetwo(factor_output)
if(testCasetwoResults != 'Factor Analysis testCasetwo Passed') {
  allResults <- c(allResults,'testCasetwo', testCasetwoResults)
}

##### Column names must be sorted in data
testCasethree <- function(data_set){
  failedTestResults <- list()
  result <- tryCatch({
    test_that("Column names are soted in data",{
      rev_data_set =data_set$xts_incoming_row [,order(ncol(data_set$xts_incoming_row ):1)]
      columns <- colnames(rev_data_set)
      if(!is.unsorted(columns)){
        testCasetwo_Check <- TRUE
      }
      expect_equal(testCasetwo_Check, TRUE)
    })
  }, error = function(e){return(e)})
  if(!isTRUE(result)){
    failedTestResults <- c(failedTestResults, as.character(result))
  }
  
  if(length(failedTestResults) == 0){
    return("Factor Analysis testCasethree Passed")
  } else {
    return(failedTestResults)
  }
}

testCasethreeResults <- testCasethree(xts_processing(data[[n.timestep]]$factorEventTime))
if(testCasethreeResults != 'Factor Analysis testCasethree Passed') {
  allResults <- c(allResults,'testCasethree', testCasethreeResults)
}

##### Data set should not contain NA values
testCasefour <- function(data_set){
  failedTestResults <- list()
  result <- tryCatch({
    test_that("Data set contains NA values or not",{
      indx <- apply(data_set$xts_df, 1, function(x) any(is.na(x)))
      if(!isTRUE(indx)){
        testCasefour_Check <- TRUE
      }
      expect_equal(testCasefour_Check, TRUE)
    })
  }, error = function(e){return(e)})
  if(!isTRUE(result)){
    failedTestResults <- c(failedTestResults, as.character(result))
  }
  
  if(length(failedTestResults) == 0){
    return("Factor Analysis testCasefour Passed")
  } else {
    return(failedTestResults)
  }
}

testCasefourResults <- testCasefour(xts_processing(data[[n.timestep]]$factorEventTime))
if(testCasefourResults != 'Factor Analysis testCasefour Passed') {
  allResults <- c(allResults,'testCasefour', testCasefourResults)
}

##### Checking amount of variance explained by factors
testCasefive <- function(factor_loadings, data){
  failedTestResults <- list()
  result <- tryCatch({
    test_that("Values for variance explained by factors do not match",{
      vector_loadings <- as.vector(data$proportionalVar)
      if(!is.null(vector_loadings)){
        testCasefive_Check <-  round(vector_loadings,3)
      }
      expect_equal(round(factor_loadings,3), testCasefive_Check)
    })
  }, error = function(e){return(e)})
  if(!isTRUE(result)){
    failedTestResults <- c(failedTestResults, as.character(result))
  }
  
  if(length(failedTestResults) == 0){
    return("Factor Analysis testCasefive Passed")
  } else {
    return(failedTestResults)
  }
}

testCasefiveResults <- testCasefive(factor_loadings, factor_output)
if(testCasefiveResults != 'Factor Analysis testCasefive Passed') {
  allResults <- c(allResults,'testCasefive', testCasefiveResults)
}

##### Checking timesteps of factor agent 1 and factor agent 2
# factor agent 1 output should be T-1 of factor agent 2
testCasesix <- function(factor_agent_1, factor_agent_2){
  failedTestResults <- list()
  result <- tryCatch({
    test_that("Timestep for Factor Agent 1 should be T-1 of Factor Agent 2",{
      expect_equal(as.integer(factor_agent_1$factorEventTime / 1000) + 5, 
                   as.integer(factor_agent_2$factorEventTime / 1000))
    })
  }, error = function(e){return(e)})
  if(!isTRUE(result)){
    failedTestResults <- c(failedTestResults, as.character(result))
  }
  
  if(length(failedTestResults) == 0){
    return("Factor Analysis testCasesix Passed")
  } else {
    return(failedTestResults)
  }
}

testCasesixResults <- testCasesix(data[[n.timestep]], factor_output)
if(testCasesixResults != 'Factor Analysis testCasesix Passed') {
  allResults <- c(allResults,'testCasesix', testCasesixResults)
}



#### Logging test results ####
# Generating summary and raising an error
if(length(allResults) == 0) {
  print("All tests passed")
} else {
  print(allResults)
}
                   















